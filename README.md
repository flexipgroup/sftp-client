# SFTP Client User Documentation

The is a file-based solution that facilitates the file transfers across systems on a Host-to-Host infrastructure

**1. Getting Started**
---------------------------
Welcome! This documentation will highlight core features of the host-to-host integration. For further details, see the links within this document, or the API documentation site ([http://8.208.24.142:8080/flexware/swagger-ui.html](http://8.208.24.142:8080/flexware/swagger-ui.html)). 

- 1.1. Components
- a. Client
- b.	Server
- c.	Flexware API
- d.	Front End (Admin/Client)

**1.1.1.	File Based Client Application**
This consists of the SFTP client application that is setup in the Corporate client’s environment. The application is an java (.jar) file  installed/deployed as a service on the operating system and carries out its required processes in the background. Once it is running, there is little or no need for any manual actions.

**1.1.2.	File Based Server Application**
There is equally a SFTP server application that is hosted on the remote banking environment. This is what the corporate client applications will be communicating with.

**1.1.3.	Flexware API**
This is a set of predefined protocols that have been designed  to manage communication and handle requests sent to the core banking application.
1.1.4.	Front End (Admin/Client)
A dashboard interface is readily available to provide visual aid to the collection of services such as client onboarding for the admin and file upload for the client.

**2.	Setting Up (File Based)**
-----------------------------
**2.1.	How it works**
The SFTP client and server applications can be easily installed by running the setup file which will automatically set up the necessary configurations needed to function. The application creates a folder (with sub-folders) on both server and client side.

**The sub folders on the client side is as follows:**

1. *Download folder:* for automatically placing file(s) downloaded via ERP
    which is automatically sent to the read folder.

2. *Read folder:* file(s) in this directory is automatically encrypted and
    added to the queue (used to process files synchronously / 
    asynchronously to be sent to the server).

3. *Receiver folder:* files in queue is automatically moved to this directory
    and sent to the server. 

4. Archive folder: is essentially a backup directory for encrypted file(s)
    that is sent to the server.

5. *Success folder:* encrypted file(s) sent successfully to the server is
    copied to this directory.

6. *Error folder:* unsent file(s) due to errors is copied into this directory.

7. *Logs folder:* records of all transactions when using the application is
    logged in this directory. 

8. Acknowledge folder: for administrative purposes only.

9. Receiver Folder: temporary location for encrypted files before they get sent to the Server.


**The sub folders on the server side is as follows:**

1. *Upload folder:* for automatically placing file(s) uploaded to the server
    which is automatically sent to the read folder.

2. *Read folder:*  file(s) copies to this directory is automatically decrypted.

3. *Receiver folder:* decrypted file(s) from read folder is automatically
    moved to this directory in the server. 

4. *Archive folder:* is essentially a backup directory for decrypted file(s)
    sent from the client.

5. *Success folder:* decrypted file(s) sent successfully from the client is
    copied to this directory.

6. *Error folder:* unsent file(s) due to errors is copied into this directory.

7. *Logs folder:* records of all transactions while using the server
    application is logged in this directory. 

8. *Acknowledge folder:* for administrative purposes only.




2.2.	Server Configuration
--
The server application is located within the UBA network and has restricted access. This requires a SSH connection to the dedicated IP via SFTP, and providing the adequate administrative credentials. 

When the application is to be run for the first time, it is essential that the configuration is done according to the specifications provided. Here’s what you’ll need;
- i.	Create a folder on the root directory with “**Flex-Server**” as the name and move/upload the config.ini and SFTPServer.jar into the *Flex-Server* folder.
- ii.	To start the process cd into the “**Flex-Server**” folder with your terminal and run this command


> `java -jar SFTPServer.jar ` 

Once the application is started, it runs continuously and doesn’t need to be stopped or restarted.
2.3.	Client Configuration
The client application is to be placed in an application folder named “Flex-Client” in any directory.  The contents of this folder include;
-	SFTPClient.jar
-	config.ini

Setup of the Client application can be easily done by following the 2 steps listed below;

**2.3.	Client onboarding (Step One)**
-
To onboard a client, the UBA Admin must log on to the Web Portal with provisioned credentials and create a client account. Once a client account is created, the client is assigned a “**Customer ID**” which is required in the *config.ini* file. 

On the client page, navigate to the “**Services**” tab, and click on “**SFTP**”.  On the right section, the *customer ID* can be found and copied.

**2.4.	Setting up the environments (Step Two)**
-
The application was built to run on Linux based operating system but can also be used in windows environment with the aid of some configuration.

**2.4.1.	Linux**
Manual Installation. 
i.	Unzip the flexclient.zip folder into the home directory e.g.
> \Users\your_username

The folder should contain the folders listed in **2** above;
ii.	With your already created folder named “**Flex-Clien**t” move the `config.ini` and `SFTPClient.jar` into the *Flex-Client* folder 
iii. Get the Server’s IP address, Customer ID, Username and Password (if any) from the Client’s page on the Web Portal and Add it to the `config.ini` file with a `[SFTP]` header.
iv.	Then connect with the server by running this on the terminal SSH ‘server username’ @ IP address” (for example [SSH FLEXware@192.168.1.206]) 
After a successful connection type “`exit`” and hit enter
v.	To start the process cd into the “**Config**” folder with your terminal and run this command

>` java -jar SFTPClient.jar ` 

2.4.2.	Windows 
Manual Installation. 
i.	Unzip the “flexclient.zip” folder into the home directory e.g. 

>  “\Users\your username” 

The folder should contain the folders listed in **2**;

ii.	With your already created folder named “**Flex-Client**” move the `config.ini` and `SFTPClient.jar` into the *Flex-Client* folder 
iii.	Get the Server’s IP address, Customer ID, Username and Password (if any) from the Client’s page on the Web Portal and Add it to the `config.ini` file with a `[SFTP]` header 
iv.	Then connect with the server by running this on the terminal SSH ‘server username’ @ IP address” (E.g [SSH FLEXware@192.168.1.206])
After a successful connection type “exit” and hit enter.
v.	To start the process cd into the “**Config**” folder with your terminal and run this command


>  `java -jar SFTPClient.jar`


Once the connection to the server has been established and the client is running, files dropped in the “Download” folder on the client machine will immediately be uploaded to the server for processing.
